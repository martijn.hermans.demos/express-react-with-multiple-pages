require("@babel/register")({
  presets: [
    "@babel/preset-react",
    [
      "@babel/env",
      {
        targets: {
          node: true
        }
      }
    ]
  ]
});
require("./server");
