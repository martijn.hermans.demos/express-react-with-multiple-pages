import { Router } from "express";
import React from "react";
import ReactDOM from "react-dom/server";
import createDocument from "./create-document";
import * as Api from "./api";
import RepositoryOverviewPage from "./views/pages/repository-overview";
import RepositoryDetailPage from "./views/pages/repository-detail";

const router = Router();
export default router;

router.get("/", async (req, res) => {
  const query = "react";

  const repositories = await Api.fetchRepositories(query);

  const html = ReactDOM.renderToString(
    <RepositoryOverviewPage repositories={repositories} query={query} />
  );

  res.send(
    createDocument({
      html
    })
  );
});

router.get("/repos/:owner/:repo", async (req, res) => {
  const { owner, repo } = req.params;

  const repository = await Api.fetchRepository(owner, repo);

  const html = ReactDOM.renderToString(
    <RepositoryDetailPage repository={repository} />
  );

  res.send(
    createDocument({
      html
    })
  );
});
