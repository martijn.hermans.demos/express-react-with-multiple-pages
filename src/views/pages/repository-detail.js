import React from "react";

export default function RepositoryDetailPage({ repository }) {
  return (
    <div className="container">
      <h3>{repository.name}</h3>
      <p>{repository.description}</p>
    </div>
  );
}
