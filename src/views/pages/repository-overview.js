import React from "react";

import RepositoryList from "../../components/repository-list";

export default function RepositoryOverviewPage({ query, repositories }) {
  return (
    <div className="container">
      <h3>{query}</h3>
      <RepositoryList repositories={repositories} />
    </div>
  );
}
